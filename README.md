# App Engine blog with CloudSQL #

Very simple django blog app running on app engine

**Features**

*  pagination (set to 2)
*  dashboard
*  tests


## Running/testing locally ##

Locally the code uses sqlite database configured in settings

1. clone and `cd` the repo
2. `mkvirtualenv appenine_blog`
3. `pip install -r appengine_blog/deploy/requirements.txt`
4. `./manage.py syncdb`
5. `./manage.py migrate`
6. `coverage run --source='.' manage.py test`
7. `coverage report`
8. `./manage.py runserver`

## Deploying to app engine ##

`<path_to_appengine>appcfg.py update <path_to_repo>/appengine-blog/`

## Appengine instance ##

http://symbolic-tape-653.appspot.com/

## Admin interface ##

Located at `/dashboard`.

Username / password: admin / XZqcVBsS

## TODO ##
* Add error pages (404, 500)
* Add some styles
* Many many more - anything you'd like