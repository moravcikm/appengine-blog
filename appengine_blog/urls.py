from django.conf.urls import patterns, include, url

from appengine_blog.apps.blog import views


urlpatterns = patterns('',
    url(r'^$',
        views.PostListView.as_view(),
        name='list'),
    url(r'^accounts/login/$',
        'django.contrib.auth.views.login', name='login'),
    url(r'^accounts/logout/$',
        'django.contrib.auth.views.logout', name='logout'),
    url(r'^blog/',
        include('appengine_blog.apps.blog.urls', namespace='blog')),
    url(r'^dashboard/',
        include('appengine_blog.apps.dashboard.urls', namespace='dashboard')),
)
