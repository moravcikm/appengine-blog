from django.conf.urls import patterns, include, url

from appengine_blog.apps.dashboard import views


urlpatterns = patterns('',
    url(r'^$',
        views.HomeView.as_view(),
        name='home'),
    url(r'blog/',
        include('appengine_blog.apps.dashboard.blog.urls', namespace='blog')),
)
