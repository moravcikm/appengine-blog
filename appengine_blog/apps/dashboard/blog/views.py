from django.views import generic
from django.db.models import get_model
from django.core.urlresolvers import reverse_lazy

from . import forms

Post = get_model('blog', 'Post')


class PostListView(generic.ListView):
    template_name = 'dashboard/blog/list.html'
    context_object_name = "posts"
    model = Post


class PostCreateView(generic.CreateView):
    model = Post
    success_url = reverse_lazy('dashboard:blog:list')
    template_name = 'dashboard/blog/create.html'
    form_class = forms.PostForm

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.save()
        return super(PostCreateView, self).form_valid(form)


class PostEditView(generic.UpdateView):
    model = Post
    success_url = reverse_lazy('dashboard:blog:list')
    template_name = 'dashboard/blog/edit.html'
    form_class = forms.PostForm

    def form_valid(self, form):
        return super(PostEditView, self).form_valid(form)


class PostDeleteView(generic.DeleteView):
    model = Post
    success_url = reverse_lazy('dashboard:blog:list')
    template_name = 'dashboard/blog/delete.html'

