from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required

from appengine_blog.apps.dashboard.blog import views


urlpatterns = patterns('',
    url(r'^list/',
        login_required(views.PostListView.as_view()),
        name='list'),
    url(r'^create/',
        login_required(views.PostCreateView.as_view()),
        name='create'),
    url(r'^edit/(?P<pk>\d+)',
        login_required(views.PostEditView.as_view()),
        name='edit'),
    url(r'^delete/(?P<pk>\d+)',
        login_required(views.PostDeleteView.as_view()),
        name='delete'),
)
