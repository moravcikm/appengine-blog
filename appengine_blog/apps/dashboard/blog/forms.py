from django import forms
from django.db.models import get_model

Post = get_model('blog', 'Post')


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['title', 'content', 'status']