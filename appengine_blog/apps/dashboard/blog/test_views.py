# Run tests with "manage.py test".

from django.test import Client
from django.test import TestCase
from django.db.models import get_model
from django.core.urlresolvers import reverse

Post = get_model('blog', 'Post')
User = get_model('auth', 'User')


class BlogPostDashboardViewTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='1', is_active=True)
        self.user.set_password('1')
        self.user.save()

        self.posts = [
            Post.objects.create(
                id=1, user=self.user, title='first', content='1', status=Post.ACTIVE),
            Post.objects.create(
                id=2, user=self.user, title='2', content='2', status=Post.ACTIVE),
            Post.objects.create(
                id=3, user=self.user, title='3', content='3', status=Post.ACTIVE),
            Post.objects.create(
                id=4, user=self.user, title='4', content='4', status=Post.INACTIVE),
        ]
        self.client = Client()

    def _login(self):
        """
        Logs in the user and we can use this in assertion of successful login
        """
        return self.client.login(username=self.user.username, password='1')

    def test_blog_post_list_no_login(self):
        response = self.client.get(reverse('dashboard:blog:list'))
        self.assertEqual(302, response.status_code)

    def test_blog_post_list_with_login(self):
        self.assertTrue(self._login())
        response = self.client.get(reverse('dashboard:blog:list'))
        self.assertEqual(len(response.context['object_list']), 4)

    def test_blog_post_create_no_login(self):
        response = self.client.get(reverse('dashboard:blog:create'))
        self.assertEqual(302, response.status_code)

    def test_blog_post_create_with_login(self):
        """
        Tests creation of the new blog post.
        Also check if user is assigned as per form_valid()
        """
        self.assertTrue(self._login())
        response = self.client.get(reverse('dashboard:blog:create'))
        self.assertEqual(200, response.status_code)
        number_of_posts_before_create = Post.objects.all().count()
        self.assertEqual(number_of_posts_before_create, 4)

        response = self.client.post(reverse('dashboard:blog:create'), {
            'title': '1',
            'content': '1',
            'status': '3',
            }, follow=True)

        self.assertEqual(200, response.status_code)
        self.assertEqual(Post.objects.all().count(), number_of_posts_before_create + 1)
        self.assertEqual(Post.objects.all()[4].user, self.user)
        self.assertEqual(Post.objects.all()[4].status, 3)

    def test_blog_post_edit_no_login(self):
        response = self.client.get(reverse('dashboard:blog:edit', args=(1,)))
        self.assertEqual(302, response.status_code)

    def test_blog_post_edit_with_login(self):
        self.assertTrue(self._login())
        response = self.client.get(reverse('dashboard:blog:edit', args=(1,)))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, 'first')

        response = self.client.post(reverse('dashboard:blog:edit', args=(1,)), {
            'title': 'second',
            'content': '1',
            'status': '2',
        }, follow=True)

        self.assertEqual(200, response.status_code)
        self.assertEqual(Post.objects.get(id=1).title, 'second')
        self.assertEqual(Post.objects.get(id=1).status, 2)

    def test_blog_post_delete_no_login(self):
        response = self.client.get(reverse('dashboard:blog:delete', args=(1,)))
        self.assertEqual(302, response.status_code)

    def test_blog_post_delete_with_login(self):
        self.assertTrue(self._login())
        response = self.client.get(reverse('dashboard:blog:delete', args=(1,)))
        self.assertEqual(200, response.status_code)

        number_of_items_before_deletion = len(self.posts)

        response = self.client.post(reverse('dashboard:blog:delete', args=(1,)), {
            }, follow=True)

        self.assertEqual(200, response.status_code)
        self.assertEqual(Post.objects.all().count(),
            number_of_items_before_deletion - 1)
