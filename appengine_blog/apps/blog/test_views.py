# Run tests with "manage.py test".

from django.test import Client
from django.test import TestCase
from django.db.models import get_model
from django.core.urlresolvers import reverse

from . import views

Post = get_model('blog', 'Post')
User = get_model('auth', 'User')


class BlogPostViewTest(TestCase):
    def setUp(self):
        user = User.objects.create(username='1')

        self.posts = [
            Post.objects.create(
                user=user, title='1', content='1', status=Post.ACTIVE),
            Post.objects.create(
                user=user, title='2', content='2', status=Post.ACTIVE),
            Post.objects.create(
                user=user, title='3', content='3', status=Post.ACTIVE),
            Post.objects.create(
                user=user, title='4', content='4', status=Post.INACTIVE),
        ]
        self.client = Client()

    def test_list_no_pagination(self):
        views.PostListView.paginate_by = None
        response = self.client.get(reverse('list'))
        self.assertEqual(200, response.status_code)
        self.assertEquals(len(response.context['object_list']), 3)
        self.assertNotIn(self.posts[3], response.context['object_list'])

    def test_list_with_pagination(self):
        views.PostListView.paginate_by = 2
        response = self.client.get(reverse('list'))
        self.assertEqual(200, response.status_code)
        self.assertEquals(len(response.context['object_list']), 2)

    def test_post_detail(self):
        response = self.client.get(self.posts[0].get_absolute_url())
        self.assertEqual(200, response.status_code)
        self.assertEquals(response.context['object'], self.posts[0])
        self.assertContains(response, self.posts[0].content)





