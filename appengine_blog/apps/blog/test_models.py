# Run tests with "manage.py test".

from django.test import TestCase
from django.db.models import get_model

Post = get_model('blog', 'Post')
User = get_model('auth', 'User')


class BlogModelTest(TestCase):
    def setUp(self):
        user = User.objects.create(username='1')

        self.posts = [
            Post.objects.create(
                user=user, title='1', content='1', status=Post.ACTIVE),
            Post.objects.create(
                user=user, title='2', content='2', status=Post.PENDING),
            Post.objects.create(
                user=user, title='3', content='3', status=Post.ACTIVE),
            Post.objects.create(
                user=user, title='4', content='4', status=Post.INACTIVE),
            ]

    def test_active_manager(self):
        active_posts = Post.objects.active()
        self.assertEqual(len(active_posts), 2)
        self.assertIn(self.posts[0], active_posts)
        self.assertIn(self.posts[2], active_posts)
        self.assertNotIn(self.posts[1], active_posts)

    def test_normal_manager(self):
        self.assertEqual(len(Post.objects.all()), 4)
