from django.views import generic
from django.db.models import get_model

Post = get_model('blog', 'Post')


class PostListView(generic.ListView):
    template_name = 'blog/list.html'
    model = Post
    context_object_name = 'posts'
    paginate_by = 2

    def get_queryset(self):
        return self.model.objects.active()


class PostDetailView(generic.DeleteView):
    model = Post
    template_name = 'blog/detail.html'
    context_object_name = 'post'