from django.conf.urls import patterns, url

from appengine_blog.apps.blog import views


urlpatterns = patterns('',
    url(r'^post/(?P<pk>\d+)',
        views.PostDetailView.as_view(),
        name='detail'),
)
