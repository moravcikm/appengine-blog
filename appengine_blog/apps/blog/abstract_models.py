from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse


class PostManager(models.Manager):
    def active(self):
        return self.model.objects.filter(status=AbstractPost.ACTIVE)


class AbstractPost(models.Model):
    PENDING, ACTIVE, INACTIVE  = 1, 2, 3
    STATUS_CHOICES = (
        (PENDING, 'Pending'),
        (ACTIVE, 'Active'),
        (INACTIVE, 'Inactive'),
    )

    user = models.ForeignKey(User)
    title = models.CharField(max_length=128)
    content = models.TextField()
    status = models.SmallIntegerField(choices=STATUS_CHOICES, default=PENDING)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True

    def __unicode__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('blog:detail', args=(self.id,))

    objects = PostManager()
